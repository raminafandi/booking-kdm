import requests
import pandas as pd
import json

# globals
url_search_hotel = "https://booking-com.p.rapidapi.com/v1/hotels/search"
url_reviews = "https://booking-com.p.rapidapi.com/v1/hotels/reviews"

# querystring for searching hotel
querystring = {"checkout_date": "2022-10-01", "units": "metric", "dest_id": "900040280", "dest_type": "city", "locale": "en-gb",
               "adults_number": "2", "order_by": "popularity", "filter_by_currency": "USD", "checkin_date": "2022-09-30", "room_number": "1"}

# headers for making api requests
headers = {
    "X-RapidAPI-Host": "booking-com.p.rapidapi.com",
    "X-RapidAPI-Key": "234950c119msh4395bd2508d03a0p16d4d4jsn070f3d556fec"
}


# querystring maker for reviews
def querystring2(hotel_id):
    return {"sort_type": "SORT_MOST_RELEVANT", "locale": "en-gb", "hotel_id": hotel_id}

\
# request maker for api requests
def make_request(url_p, querystring_p):
    return requests.request("GET", url_p, headers=headers, params=querystring_p)


# gets hotels which meets requirements of querystring
def get_search_hotels():
    response_search_hotels = make_request(
        url_search_hotel, querystring)  # make request
    dict_search_hotels = json.loads(
        response_search_hotels.text)  # parse response
    return pd.DataFrame.from_records(dict_search_hotels['result'], columns=["hotel_id", "hotel_name", "url", "class", "latitude", "longitude"])


def get_reviews(df_hotels):
    # create empty dataframe
    df = pd.DataFrame(columns=["hotel_id",  "pros",
                      "cons", "create_date", "author_id"])
    for i in range(len(df_hotels)):
        response2 = make_request(url_reviews, querystring2(
            df_hotels.iloc[i].hotel_id))  # make request for each hotel_id
        dict2 = json.loads(response2.text)  # parse response
        for j in range(len(dict2['result'])):
            df = pd.concat([df, pd.DataFrame([{"hotel_id": df_hotels.iloc[i].hotel_id, "pros": dict2['result'][j]['pros'], "cons": dict2['result'][j]['cons'],
                                               "create_date": dict2['result'][j]['date'], "author_id": str(dict2['result'][j]['author']['user_id'])}])])  # adds review to dataframe
    return df


def main():
    df_search_hotels = get_search_hotels()  # get hotels from search
    # print("Length of hotels",  len(df_search_hotels))
    # get reviews of hotels which are searched
    df_reviews = get_reviews(df_search_hotels)
    pd.merge(df_search_hotels, df_reviews, how='left', on='hotel_id').to_csv(
        'hotels.csv')  # merge search and reviews and save to csv


main()
